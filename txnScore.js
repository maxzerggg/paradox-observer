(async()=>{
	"use strict";

	const {NetEvtServer} 	= require( 'netevt' );
	const worker  			= require( 'worker_threads' );
	const protocol			= require( './protocol');
	const fs				= require( 'fs' );
	const mongo				= require( 'mongointerface' )
	const moment			= require( 'moment' );

	// global variable
	const {gatherId, listen, logPath} = worker.workerData;

	let g_record		= {txnCount:0, txnType:0, nodeAmount:0};
	let g_queue			= [];
	let g_mongoDB		= {connect:await mongo.connect("localhost", 27017), col:null};
	let g_logTxnScore 	= fs.openSync(`${logPath}/Log_${gatherId}_txnScore.txt`, "a");



	// mongoDB init
	if( !g_mongoDB.connect ){
		throw new Error('db connect is fail...');
	}
	g_mongoDB.col = await mongo.initDB("txnScore").collection(logPath);
	g_mongoDB.col.createIndex("txnHash", {background:true, unique:true});

	worker.parentPort.on("message", (jsonStr)=>{
		const {type, data} = JSON.parse(jsonStr);
		switch (type) {
			case protocol.gather_2_txnScore.SNAPSHOT_START:
			{
				log(g_logTxnScore, `[gather] SNAPSHOT_START`);
				let timeStart = Date.now();

				let score = getTxnScore();
				let {batchId} = data;
				let memory = Math.round((process.memoryUsage().rss)/1000)/1000;
				log(g_logTxnScore, `送 TXN_SCORE_REPORT 給 [gather] batchId: ${batchId}, txnScore(${score}), ${Date.now()-timeStart}(ms) ${memory}(MB)`);
				worker.parentPort.postMessage(JSON.stringify({type:protocol.txnScore_2_gather.TXN_SCORE_REPORT, data:{result:` snapshot[${___EPOCH_TO_LOCAL(batchId)}]: txnScore(${score}), ${Date.now()-timeStart}(ms) ${memory}(MB)`}}));
			}
				break;
		}

		function ___EPOCH_TO_LOCAL(epoch){
			return moment(epoch).format('YYYY-MM-DD hh-mm-ss');
		}
	});

	const nodeServer = new NetEvtServer();
	nodeServer
		.on("connected", (e)=>{
			const {type, sender, rawData} = e;
			log(g_logTxnScore, `node connected!`);
		})
		.on("disconnected", async(e)=>{
			const {type, sender, rawData} = e;
			log(g_logTxnScore, `node disconnected! ${sender.id}`);

			g_queue.push({fun:work_removeNode, args:{nodeId:sender.id}});
		})
		.on(protocol.node_2_txnScore.ADD_NODE, (e)=>{
			const {type, sender, rawData} = e;
			const {nodeId} = JSON.parse(rawData);
			log(g_logTxnScore, `[node] ADD_NODE, nodeId:${nodeId}`);
			// log(g_logNodeStream, `[node][${.id.substr(35, 61);}] SNAPSHOT_READY_ACK, batchId:${batchId}`);
			sender.id = nodeId.substr(34, 61);

			g_queue.push({fun:work_addNode, args:null});
		})
		.on(protocol.node_2_txnScore.ADD_TXN, async(e)=>{
			const {type, sender, rawData} = e;
			const {txnHash} = JSON.parse(rawData);
			log(g_logTxnScore, `[node_${sender.id}] ADD_TXN, txnHash:${txnHash}`);

			g_queue.push({fun:work_addTxn, args:{nodeId:sender.id, txnHash}});
		})
		.listen(...listen);

	processWork();


	function getTxnScore(){
		let denominator = g_record.nodeAmount*g_record.txnType;
		return denominator > 0 ? Math.round( (1000*g_record.txnCount) / denominator )/1000 : 0;
	}
	async function processWork(){
		if( g_queue.length > 0 ){
			let w = g_queue.shift();
			await w.fun(w.args);
		}
		setTimeout(processWork, 0);
	}

	async function work_addNode(){
		return new Promise(async(resolve)=>{
			g_record.nodeAmount++;

			log(g_logTxnScore, `[g_record] txnCount(${g_record.txnCount}) nodeAmount(${g_record.nodeAmount}) txnType(${g_record.txnType})`);
			resolve();
		});
	}
	async function work_removeNode(args){
		let {nodeId} = args;
		return new Promise(async(resolve)=>{
			// await g_mongoDB.col.updateMany({},  { $pull: {nodes: nodeId} });// 清除db中的紀錄
			// g_record.nodeAmount--;

			log(g_logTxnScore, `[g_record] txnCount(${g_record.txnCount}) nodeAmount(${g_record.nodeAmount}) txnType(${g_record.txnType})`);
			resolve();
		});
	}
	async function work_addTxn(args){
		let {nodeId, txnHash} = args;
		return new Promise(async(resolve)=>{
			let result = await g_mongoDB.col.findOneAndUpdate({txnHash:txnHash},  { $addToSet: {nodes: nodeId} }, { upsert: true });
			g_record.txnType += (result.lastErrorObject.updatedExisting ? 0 : 1);
			g_record.txnCount ++;

			log(g_logTxnScore, `[g_record] txnCount(${g_record.txnCount}) nodeAmount(${g_record.nodeAmount}) txnType(${g_record.txnType})`);
			resolve();
		});
	}
	function log(logObj, ...args){
		let t = new Date();
		let month = t.getMonth()+1;
		month = month.toString().padStart(2, 0);
		let tStr = `[${t.getFullYear()}-${month}-${t.getDate().toString().padStart(2, 0)} ${t.getHours().toString().padStart(2, 0)}:${t.getMinutes().toString().padStart(2, 0)}:${t.getSeconds().toString().padStart(2, 0)}.${t.getMilliseconds().toString().padStart(3, 0)}]`;

		for(let arg of args){
			tStr += arg;
		}
		// console.log(tStr);
		fs.appendFileSync(logObj, tStr+'\r\n', 'utf8');
	}
})();