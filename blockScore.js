(()=>{
	"use strict";

	const worker  			= require( 'worker_threads' );
	const {LevelKV}			= require( 'levelkv' );
	const path 				= require( 'path' );
	const fs				= require( 'fs' );
	const protocol			= require( './protocol');
	const moment			= require( 'moment' );
	const { UInt64, Binary, Serialize, Deserialize } = require('beson');

	const PROGRESS_INTERVAL = 60000;
	const ZERO_HASH 	 	= Binary.alloc(32);

	// global variable
	const {threadId, logPath} = worker.workerData;
	let g_levelDB 			= null;
	let g_leafs 			= {};
	let g_nodes				= {};
	let g_blockTree			= {};
	let g_progress  		= {};
	let g_logBlockScore 	= fs.openSync(`${logPath}/Log_blockScore_#${threadId}.txt`, "a");

	try{
		worker.parentPort.on("message", async (jsonStr)=>{
		const {type, data} = JSON.parse(jsonStr);
		switch (type) {
			case protocol.summary_2_blockScore.ASSIGN_JOB:
			{
				try{
					log(g_logBlockScore, ``);
					log(g_logBlockScore, ``);
					log(g_logBlockScore, ``);
					const {batchId, dbPath, totalNodesNum} = data;
					let t0 = await __COMBINE_LEVELKV(dbPath);
					log(g_logBlockScore, `snapshot(${___EPOCH_TO_LOCAL(batchId)})===> __COMBINE_LEVELKV() cost ${t0}(ms)`);

					let t1 = await __STATISTICS();
					log(g_logBlockScore, `snapshot(${___EPOCH_TO_LOCAL(batchId)})===> __STATISTICS() cost ${t1}(ms)`);

					let leafArray = [];
					let t2 = await __BUILD_TREE(leafArray);
					log(g_logBlockScore, `snapshot(${___EPOCH_TO_LOCAL(batchId)})===> __BUILD_TREE() cost ${t2}(ms)`);

					let result = {};
					let leafNum = leafArray.length;
					let t3 = await __ANALYSIS(leafArray, result);
					log(g_logBlockScore, `snapshot(${___EPOCH_TO_LOCAL(batchId)})===> __ANALYSIS() cost ${t3}(ms), ${leafNum} leaf`);
					log(g_logBlockScore, `snapshot(${___EPOCH_TO_LOCAL(batchId)})===> finalScore(${result.score}), ${t0+t1+t2+t3}(ms) ${result.memory}(MB)`);

					await g_levelDB.close();
					g_levelDB 	= null;
					g_leafs 	= {};
					g_nodes		= {};
					g_blockTree	= {};
					await _idle(1);
					fs.renameSync(dbPath, `${dbPath}___[done]`);

					log(g_logBlockScore, `送 BLOCK_SCORE_REPORT 給 [summary] batchId: ${___EPOCH_TO_LOCAL(batchId)}`);
					worker.parentPort.postMessage(JSON.stringify({type:protocol.blockScore_2_summary.BLOCK_SCORE_REPORT, data:{threadId, result:` snapshot[${___EPOCH_TO_LOCAL(batchId)}]: totalNodesNum(${totalNodesNum}), blockScore(${result.score}), ${t0+t1+t2+t3}(ms) ${result.memory}(MB)`}}));
				}catch(err){
					console.log(err);
				}
			}
			break;
		}

		async function __COMBINE_LEVELKV(batchIdPath){
			try{
				let timeStart = Date.now();
				g_levelDB = await LevelKV.initFromPath(batchIdPath);
				g_levelDB.initPath = batchIdPath;
				const dirs = p => fs.readdirSync(p).filter(f => fs.statSync(path.join(p, f)).isDirectory());
				let dirNameArray = dirs(batchIdPath);
				// log(g_logBlockScore, `__COMBINE_LEVELKV dirs: ${dirNameArray}`);
				___PROGRESS_START("__COMBINE_LEVELKV", dirNameArray.length, PROGRESS_INTERVAL);
				for(let v of dirNameArray){
					___PROGRESS_ADD("__COMBINE_LEVELKV");
					let gatherPath = `${batchIdPath}/${v}`;
					// log(g_logBlockScore, `__COMBINE_LEVELKV gatherPath: ${gatherPath}`);
					let gatherDB = await LevelKV.initFromPath(gatherPath);
					gatherDB.initPath = gatherPath;
					let gatherDBCursor = await gatherDB.get();
					// log(g_logBlockScore, `__COMBINE_LEVELKV gatherDBCursor(${gatherDBCursor.length})`);
					for await (let node of gatherDBCursor){
						await g_levelDB.put(node.id, node);
					}
					await gatherDB.close();
				}
				___PROGRESS_END("__COMBINE_LEVELKV");
				return Date.now() - timeStart;
			}catch(err){
				console.log("blockScore error");
				console.log(err);
			}
		}
		async function __STATISTICS(){
			let timeStart = Date.now();
			let levelDBCursor = await g_levelDB.get();
			___PROGRESS_START("__STATISTICS", levelDBCursor.size, PROGRESS_INTERVAL);
			for await (let node of levelDBCursor){
				___PROGRESS_ADD("__STATISTICS");
				let addNodesNum = false;
				let blockHash 	= node.latest;
				if( !g_nodes.hasOwnProperty(node.id) && blockHash ){
					g_nodes[node.id] = blockHash;
					addNodesNum = true;
					// logger.Log(`    [thread]batchId(${batchId})nodeHash(${nodeHash}) latest(${blockHash})`);
				}

				// 檢查 leaf 有無存在
				let leaf = g_leafs.hasOwnProperty(blockHash) ? g_leafs[blockHash] : {hash:blockHash, nodesNum:0, blocks:{}};
				leaf.nodesNum = addNodesNum ? leaf.nodesNum+1 : leaf.nodesNum;
				let blocks = node.blocks;
				for(let b in blocks){
					if( !leaf.blocks.hasOwnProperty(b) ){
						leaf.blocks[b] = Binary.from( blocks[b].parent._ab );
					}
				}
				g_leafs[blockHash] = leaf;
			}
			// log(g_logBlockScore, `__STATISTICS g_leafs(${Object.keys(g_leafs).length}): `, g_leafs);
			___PROGRESS_END("__STATISTICS");
			return Date.now() - timeStart;
		}
		function __BUILD_TREE(out_leafArray=[]){
			let timeStart = Date.now();
			let i = 0;
			log(g_logBlockScore, `__BUILD_TREE g_leafs(${Object.keys(g_leafs).length}): `, g_leafs);
			___PROGRESS_START("__BUILD_TREE", Object.keys(g_leafs).length, PROGRESS_INTERVAL);
			for(let lh in g_leafs){
				___PROGRESS_ADD("__BUILD_TREE");
				// g_leafs.blocks 轉換成 array
				let leaf = g_leafs[lh];
				// log(g_logBlockScore, `__BUILD_TREE leaf: `, leaf);
				leaf.index = i++;
				let pathArray = [];
				let hash = leaf.hash;

				// logger.Log(leaf);
				// log(g_logBlockScore, `__BUILD_TREE 第一筆hash: `, hash);
				while( hash != ZERO_HASH.toString() ){
					pathArray.push(hash);
					hash = leaf.blocks[hash];
					// log(g_logBlockScore, `__BUILD_TREE next hash: `, hash.toString());
				}
				pathArray = pathArray.reverse();
				leaf.blocks = pathArray.slice();
				// log(g_logBlockScore, `__BUILD_TREE leaf.blocks: `, leaf.blocks);

				// build tree
				let blockTreeIterator = g_blockTree;
				let leafId = "";
				while( pathArray.length > 0 ){
					hash = pathArray.shift();
					// log(g_logBlockScore, `__BUILD_TREE hash: `, hash);
					let value = null;
					// logger.Info(hash);
					if( !blockTreeIterator.hasOwnProperty(hash) ){
						let copyV = blockTreeIterator.v;
						delete blockTreeIterator.v;

						// case1: 都空的
						if( Object.keys(blockTreeIterator).length === 0 ){ value = 1; }

						// case2: 有東西
						else{
							value = 0;
							for(let h in Object.keys(blockTreeIterator)){
								if( hash === h ){
									value = blockTreeIterator[hash].v;
									break;
								}
							}
						}
						blockTreeIterator.v = copyV;
						blockTreeIterator[hash] = {v:value};
					}else{
						value = blockTreeIterator[hash].v;
					}

					leafId += value;
					blockTreeIterator = blockTreeIterator[hash];
				}
				// logger.Log(`    [thread]batchId(${batchId})L_${leaf.index} build tree, ${Date.now()-time1}(ms)`);
				leaf.id = leafId;

				// 再把 g_leafs 轉換成 array
				out_leafArray.push(leaf)
			}
			___PROGRESS_END("__BUILD_TREE");
			g_leafs = null;
			return Date.now() - timeStart;
		}
		async function __ANALYSIS(leafArray, out_result={}){
			log(g_logBlockScore, `__ANALYSIS leafArray: `, leafArray);
			let score = 0;
			let denominator = 0;
			let timeStart = Date.now();
			// logger.Log(`    [thread]總共 ${leafArray.length} leaf`);
			___PROGRESS_START("__ANALYSIS", leafArray.length, PROGRESS_INTERVAL);
			while( leafArray.length > 0 ){
				___PROGRESS_ADD("__ANALYSIS");
				let timeStart2 = Date.now();
				let leaf = leafArray.shift();
				let myLength = leaf.id.length;

				// step 2.1: 自己配對
				let score1 = 0
				if( leaf.nodesNum > 1 ){
					score1 = ___COMBINATORICS_BY_2(leaf.nodesNum);
					denominator += score1;
				}

				// step 2.2: 和別人配對
				let score2 = 0;
				for(let l of leafArray){
					let otherLength = l.id.length;
					let maxPath = Math.max(myLength, otherLength);
					let maxSamePath = await ___DIVIDE_AND_CONQUER(leaf.id, l.id);
					score2 += maxSamePath*leaf.nodesNum*l.nodesNum/maxPath;
					denominator += leaf.nodesNum*l.nodesNum;
				}
				log(g_logBlockScore, `    [thread](L_${leaf.index}, ${leaf.nodesNum}) score1(${score1}) score2(${score2}) , ${Date.now()-timeStart2}(ms)`);
				score += score1 + score2;
			}
			___PROGRESS_END("__ANALYSIS");
			out_result.score = denominator > 0 ? Math.round(1000*score/denominator)/1000 : 0;
			out_result.memory = Math.round((process.memoryUsage().rss)/1000)/1000;
			return Date.now() - timeStart;
		}

		function ___DIVIDE_AND_CONQUER(Astring, Bstring, cut=null, lastCompare=null){
			// console.log(`hi`);
			return new Promise((resolve)=>{
				setTimeout(async ()=>{
					let h = Math.min( Astring.length, Bstring.length );
					let nowLeft = lastCompare ? lastCompare.left : 0;
					let nowRight = lastCompare ? lastCompare.right : h+1;

					cut = cut ? cut : Math.floor( h/2 );
					// console.log(`${runTimes++} cut(${cut})`);
					let tmpAstring = Astring.slice(0, cut);
					let tmpBstring = Bstring.slice(0, cut);
					let result = Buffer.compare( Buffer.from(tmpAstring), Buffer.from(tmpBstring) ) === 0;
					// console.log(`${runTimes++} cut(${cut}) result(${result})`);

					let newCut 		= null;
					let lastCut 	= lastCompare ? lastCompare.lastCut : null;
					let lastResult 	= lastCompare ? lastCompare.lastResult : null;

					if( result ){
						nowLeft = cut;
						newCut = Math.floor((cut+nowRight)/2);
						newCut = newCut === cut ? newCut+1 : newCut;
						if( newCut >= nowRight ){
							resolve(cut);
							return;
						}
					}
					else{
						nowRight = cut;
						newCut = Math.floor((cut+nowLeft)/2);
						newCut = newCut === cut ? newCut-1 : newCut;
						if( newCut === nowLeft ){
							if( lastCompare && lastResult ){
								resolve(lastCut);
								return;
							}
							resolve(0);
							return;
						}
					}

					let r = await ___DIVIDE_AND_CONQUER(Astring, Bstring, newCut, {lastCut:cut, lastResult:result, left:nowLeft, right:nowRight});
					resolve(r);
					return;
				}, 0);
			});
		}
		function ___COMBINATORICS_BY_2(m){
			if( m < 2 ){
				throw new Error(`combinatorics Error: m(${m}) is less than n(2)`);
			}

			return (m-1)*m/2;
		}
		function ___PROGRESS_START(key, total, time=600000){
			g_progress[key] = {total, count:0, timer:null};
			log(g_logBlockScore, `  ${key} ${g_progress[key].count}/${g_progress[key].total} (${Math.round(g_progress[key].count*100000/g_progress[key].total)/1000}%)`);
			let t = setInterval(()=>{
				log(g_logBlockScore, `  ${key} ${g_progress[key].count}/${g_progress[key].total} (${Math.round(g_progress[key].count*100000/g_progress[key].total)/1000}%)`);
			}, time)// 10 min
			g_progress[key].timer = t;
		}
		function ___PROGRESS_ADD(key, add=1){
			g_progress[key].count += add;
		}
		function ___PROGRESS_END(key){
			log(g_logBlockScore, `  ${key} ${g_progress[key].count}/${g_progress[key].total} (${Math.round(g_progress[key].count*100000/g_progress[key].total)/1000}%)`);
			clearInterval(g_progress[key].timer);
			delete g_progress[key];
		}
		function ___EPOCH_TO_LOCAL(epoch){
			return moment(epoch).format('YYYY-MM-DD hh-mm-ss');
		}
		function log(logObj, ...args){
			let t = new Date();
			let month = t.getMonth()+1;
			month = month.toString().padStart(2, 0);
			let tStr = `[${t.getFullYear()}-${month}-${t.getDate().toString().padStart(2, 0)} ${t.getHours().toString().padStart(2, 0)}:${t.getMinutes().toString().padStart(2, 0)}:${t.getSeconds().toString().padStart(2, 0)}.${t.getMilliseconds().toString().padStart(3, 0)}]`;

			for(let arg of args){
				tStr += arg;
			}
			// console.log(tStr);
			fs.appendFileSync(logObj, tStr+'\r\n', 'utf8');
		}
		function _idle(second){
			return new Promise((resolve)=>{
				setTimeout(()=>{
					resolve();
				}, Math.floor(second*1000));
			});
		}
	})
	}catch(err){
		console.log(`!!! blockScore exception`);
		console.log(err);
	}
})();