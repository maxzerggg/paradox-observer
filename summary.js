(async()=>{
	"use strict";

	const {NetEvtServer} 		= require( 'netevt' );
	const worker  				= require( 'worker_threads' );
	const protocol				= require( './protocol');
	const fs					= require( 'fs' );
	const moment				= require( 'moment' );


	const BLOCK_SCORE_THREAD_NUM 	= 1;
	const SAVE_PATH 				= `observerData`;
	const SNAPSHOT_TIMEOUT 			= 10000;
	const WATCHER_INTERVAL 			= 60000;
	const GATHER_SERVER_PORT 		= 42050;
	const HANDLER_PORT_START 		= 40000;

	// global variable
	const g_gathers 				= [];
	const g_watcher 				= new Map();
	const g_idleBlockScoreThread 	= [];
	const g_workBlockScoreThread 	= new Set();
	let g_snapshot 					= {};
	let g_snapshotTimer				= null;
	let g_logPath					= `LOG/${moment().format('YYYY-MM-DD hh-mm-ss')}`;
	await ___CREATE_DIR(g_logPath);
	let g_logSummary				= fs.openSync(`${g_logPath}/Log_summary.txt`, "a");
	let g_logWatch					= fs.openSync(`${g_logPath}/Log_watche.txt`, "a");
	let g_logReport					= fs.openSync(`${g_logPath}/Log_report.txt`, "a");



	setInterval(()=>{
		let watchId = Date.now();
		g_watcher.set(watchId, {totalHandlersNum:g_gathers.length, handlersNum:0, batch: new Map(), timeStart: Date.now()})
		for(let v of g_gathers){
			log(g_logSummary, `送 WATCHER_REQ 給 [gather_${v.id}]的[handler] watchId: ${watchId}`);
			v.handler.postMessage(JSON.stringify({type:protocol.summary_2_gatherHandler.WATCHER_REQ, data:{watchId}}));
		}
	}, WATCHER_INTERVAL);



	try{
		// create blockScore
		log(g_logSummary, `建立 ${BLOCK_SCORE_THREAD_NUM} 個 blockScore`);
		for(let i=0; i<BLOCK_SCORE_THREAD_NUM; i++){
			let thread = new worker.Worker("./blockScore.js", {workerData: {threadId:i, logPath:g_logPath}});
			g_idleBlockScoreThread.push(thread);
			thread.on("message", (jsonStr)=>{
				const {type, data} = JSON.parse(jsonStr);
				switch (type) {
					case protocol.blockScore_2_summary.BLOCK_SCORE_REPORT:
					{
						const {threadId, result} = data;
						log(g_logSummary, `[blockScore_${threadId}] BLOCK_SCORE_REPORT`);
						log(g_logReport, result);
						g_workBlockScoreThread.delete(thread);
						g_idleBlockScoreThread.push(thread);
					}
						break;
				}
			})
		}


		// create server
		const gatherServer = new NetEvtServer();

		gatherServer
			.on("connected", (e)=>{
				const {type, sender, rawData} = e;
				log(g_logSummary, `gather connected!`);

				g_gathers.push(sender);
				log(g_logSummary, `g_gathers.size(${g_gathers.length})`);

				if( !g_snapshotTimer ){
					g_snapshotTimer = setTimeout(snapshotWork, SNAPSHOT_TIMEOUT);
				}
			})
			.on("disconnected", async(e)=>{
				const {type, sender, rawData} = e;
				log(g_logSummary, `gather disconnected! ${sender.id}`);

				let index = g_gathers.indexOf(sender);
				if( index > -1){
					let removeGather = g_gathers.splice(index, 1);
					removeGather[0].handler.postMessage(JSON.stringify({type:protocol.summary_2_gatherHandler.GATHER_OFF}));
					log(g_logSummary, `remove ${sender.id} successful`);
				}else{
					log(g_logSummary, `!!! 不該發生... ${sender.id}`);
				}

				if( g_gathers.length === 0 && g_snapshotTimer ){
					clearTimeout( g_snapshotTimer );
					g_snapshotTimer = null;
				}
			})
			.on("data", (e)=>{})
			.on(protocol.gather_2_summary.GATHER_REGISTER_REQ, async(e)=>{
				const {type, sender, rawData} = e;
				const {gatherId, logPath} = JSON.parse(rawData);
				log(g_logSummary, `[${gatherId}] GATHER_REGISTER_REQ`);
				sender.id = gatherId;
				sender.logPath = logPath;
				await ___CREATE_DIR(`${g_logPath}/${logPath}`);

				// create gatherHandler
				let listenPort = g_gathers.length + HANDLER_PORT_START;
				let thread = new worker.Worker("./gatherHandler.js", {workerData: {gatherId, listenPort, savePath:SAVE_PATH, logPath:`${g_logPath}/${logPath}`}});
				sender.handler = thread;

				thread.on("message", (jsonStr)=>{
					const {type, data} = JSON.parse(jsonStr);
					log(g_logSummary, `[handler][${gatherId}] message: ${type}`);
					switch (type) {
						case protocol.gatherHandler_2_summary.WATCHER_ACK:
						{
							const {watchId, gatherId, watcher} = data;
							log(g_logSummary, `[handler][${gatherId}] WATCHER_ACK, watchId: `, watchId);
							log(g_logSummary, `[handler][${gatherId}] WATCHER_ACK, gatherId: `, gatherId);
							log(g_logSummary, `[handler][${gatherId}] WATCHER_ACK, watcher: `, watcher);

							let watchData = g_watcher.get(watchId);
							log(g_logSummary, `[handler][${gatherId}] WATCHER_ACK, watchData: `, watchData);
							for(let batchId in watcher){
								let batch = watchData.batch.get(batchId) || new Map();
								batch.set(gatherId, watcher[batchId]);
								if( !watchData.batch.has(batchId) ){
									watchData.batch.set(batchId, batch);
								}
							}

							watchData.handlersNum += 1;
							log(g_logSummary, `[handler][${gatherId}] WATCHER_ACK, handlersNum(${watchData.handlersNum}), totalHandlersNum(${watchData.totalHandlersNum})`);
							if( watchData.handlersNum === watchData.totalHandlersNum ){
								let passTime = Math.round((Date.now() - watchData.timeStart)/1000);
								for(let v of watchData.batch){
									log(g_logWatch, "");
									log(g_logWatch, "");
									log(g_logWatch, `--watchId(${___EPOCH_TO_LOCAL(watchId)}), ${passTime} sec`);
									log(g_logWatch, ___SPACE_STR(6)+`snapshot(${___EPOCH_TO_LOCAL(Number(v[0]))}): [`);
									for(let u of v[1]){
										let gatherId = u[0];
										const {nodesTotalNum, nodesNum, passTime} = u[1];
										log(g_logWatch, ___SPACE_STR(12)+`${gatherId}: ${nodesNum}/${nodesTotalNum} , ${passTime} sec`);
									}
									log(g_logWatch, ___SPACE_STR(6)+`]`);
								}

								g_watcher.delete(watchId);
							}
						}
							break;
						case protocol.gatherHandler_2_summary.SNAPSHOT_END:
						{
							const {batchId, gatherId, nodesNum} = data;
							log(g_logSummary, `[handler][${gatherId}] SNAPSHOT_END`);

							// let snapshot = g_snapshot.get(batchId);
							g_snapshot.handlersNum += 1;
							g_snapshot.totalNodesNum += nodesNum;
							log(g_logSummary, `[handler][${gatherId}] SNAPSHOT_END, handlersNum(${g_snapshot.handlersNum}), totalHandlersNum(${g_snapshot.totalHandlersNum}), g_idleBlockScoreThread(${g_idleBlockScoreThread.length})`);
							if( g_snapshot.handlersNum === g_snapshot.totalHandlersNum ){
								let snapshotAgainTime = SNAPSHOT_TIMEOUT - (Date.now() - g_snapshot.batchId);
								snapshotAgainTime = snapshotAgainTime > 0 ? snapshotAgainTime : 0;// 如果已經經過夠久就馬上啟動
								g_snapshotTimer = setTimeout(snapshotWork, snapshotAgainTime);

								if( g_idleBlockScoreThread.length > 0 ){
									let thread = g_idleBlockScoreThread.shift();
									let dbPath = SAVE_PATH+`/snapshot_${___EPOCH_TO_LOCAL(batchId)}`;
									thread.postMessage(JSON.stringify({type:protocol.summary_2_blockScore.ASSIGN_JOB, data:{batchId, dbPath, totalNodesNum:g_snapshot.totalNodesNum}}));
									g_workBlockScoreThread.add(thread);
								}
							}
						}
							break;
					}
					function ___SPACE_STR(n){
						let str = "";
						for(let i=0; i<n; i++){
							str += " ";
						}
						return str;
					}
				})
				.on("exit", ()=>{
					_idle(1);
					let path = `${g_logPath}/${logPath}`;
					fs.renameSync(path, `${path}___[off]`);
				});

				sender.triggerEvent(protocol.summary_2_gather.GATHER_REGISTER_ACK, JSON.stringify({handlerPort: listenPort, logPath:`${g_logPath}/${logPath}`}));
			})
			.on(protocol.gather_2_summary.TXN_SCORE_REPORT, (e)=>{
				const {type, sender, rawData} = e;
				const {result} = JSON.parse(rawData);
				log(g_logSummary, `[gather_${sender.id}] TXN_SCORE_REPORT`);

				log(g_logReport, result);
			})
			.listen(GATHER_SERVER_PORT, "0.0.0.0");
	}catch(err){
		console.log(`!!! summary exception`);
		console.log(err);
	}
	console.log(`summary server is working...`);

	function snapshotWork(){
		log(g_logSummary, `snapshotWork totalHandlersNum:${g_gathers.length}`);
		let batchId = Date.now();
		g_snapshot = {batchId, totalHandlersNum:g_gathers.length, handlersNum:0, totalNodesNum:0};
		let rawData = JSON.stringify({batchId});
		for(let v of g_gathers){
			log(g_logSummary, `送 SNAPSHOT_NOTIFY 給 [gather_${v.id}]的[handler] batchId: ${batchId}`);
			v.handler.postMessage(JSON.stringify({type:protocol.summary_2_gatherHandler.SNAPSHOT_NOTIFY, data:{batchId}}));
			log(g_logSummary, `送 SNAPSHOT_NOTIFY 給 [gather_${v.id}]`);
			v.triggerEvent(protocol.summary_2_gather.SNAPSHOT_NOTIFY, rawData);
		}
	}

	function ___CREATE_DIR (path, level=0){
		return new Promise(async(resolve)=>{
			let temp = path.split('/');
			if( level >= temp.length ){
				resolve();
				return;
			}

			let dir = "";
			for(let i=0; i<=level; i++){
				let mark = i===0 ? "" : "/";
				dir = dir+mark+temp[i];
			}
			if( !fs.existsSync(dir) ){
				fs.mkdirSync(dir);
			}
			await ___CREATE_DIR(path, level+1);
			resolve();
		});
	}
	function ___EPOCH_TO_LOCAL(epoch){
		return moment(epoch).format('YYYY-MM-DD hh-mm-ss');
	}
	function _idle(second){
		return new Promise((resolve)=>{
			setTimeout(()=>{
				resolve();
			}, Math.floor(second*1000));
		});
	}

	function log(logObj, ...args){
		let t = new Date();
		let month = t.getMonth()+1;
		month = month.toString().padStart(2, 0);
		let tStr = `[${t.getFullYear()}-${month}-${t.getDate().toString().padStart(2, 0)} ${t.getHours().toString().padStart(2, 0)}:${t.getMinutes().toString().padStart(2, 0)}:${t.getSeconds().toString().padStart(2, 0)}.${t.getMilliseconds().toString().padStart(3, 0)}]`;

		for(let arg of args){
			tStr += arg;
		}
		// console.log(tStr);
		fs.appendFileSync(logObj, tStr+'\r\n', 'utf8');
	}
})();