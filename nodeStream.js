(()=>{
	"use strict";

	const {NetEvtClient, NetEvtServer} 	= require( 'netevt' );
	const worker  			= require( 'worker_threads' );
	const {LevelKV}			= require( 'levelkv' );
	const protocol			= require( './protocol');
	const fs				= require( 'fs' );
	const util				= require( 'util' );
	const { UInt64, Binary, Serialize, Deserialize } = require('beson');

	const ZERO_HASH 	 	= Binary.alloc(32);

	const {gatherId, connection, listen, logPath} = worker.workerData;
	// global variable
	let g_gathHandler 	= null;
	let g_nodes 		= [];
	let g_nodesTemp 	= [];
	let g_snapshot 		= {};
	let g_logNodeStream = fs.openSync(`${logPath}/Log_${gatherId}_nodeStream.txt`, "a");

	try{
		worker.parentPort.on("message", (jsonStr)=>{
			const {type, data} = JSON.parse(jsonStr);
			switch (type) {
				case protocol.gather_2_nodeStream.SNAPSHOT_START:
				{
					const {batchId} = data;
					log(g_logNodeStream, `[gather] SNAPSHOT_START, batchId: ${batchId}`);
					let nodes = new Set();
					for(let v of g_nodes){
						nodes.add(v.id);
						log(g_logNodeStream, `送 LOCK_NODE_REQ 給 [node_${v.id}] batchId: ${batchId}`);
						v.triggerEvent(protocol.nodeStream_2_node.LOCK_NODE_REQ, JSON.stringify({echo:{batchId}}));
					}
					if( g_nodes.length === 0 ){
						log(g_logNodeStream, `目前沒有node, 直接送 SNAPSHOT_READY_REQ 給 [handler] batchId: ${batchId}`);
						g_gathHandler.triggerEvent(protocol.nodeStream_2_gatherHandler.SNAPSHOT_READY_REQ, JSON.stringify({batchId}));
					}
					g_snapshot[batchId] = {nodes, statesMap:new Map()};
				}
				break;
			}
		});


		const handlerClient = new NetEvtClient();
		handlerClient
		.on("connected", (e)=>{
			const {type, sender, rawData} = e;
			log(g_logNodeStream, `handler connected! `);
			g_gathHandler = sender;

			for(let v of g_nodesTemp){
				v.triggerEvent(protocol.nodeStream_2_node.SERVER_OK);
			}
			g_nodesTemp = null;
		})
		.on(protocol.gatherHandler_2_nodeStream.SNAPSHOT_READY_ACK, async (e)=>{
			const {type, sender, rawData} = e;
			const {batchId, result} = JSON.parse(rawData);
			log(g_logNodeStream, `[handler] SNAPSHOT_READY_ACK, batchId:${batchId}`);

			if(result){
				let nodesMap = g_snapshot[batchId].statesMap;
				log(g_logNodeStream, `送 SNAPSHOT_START 給 [handler] batchId: ${batchId}, nodesNum: ${nodesMap.size}`);
				g_gathHandler.triggerEvent(protocol.nodeStream_2_gatherHandler.SNAPSHOT_START, JSON.stringify({batchId, nodesNum:nodesMap.size}));
				for(let v of nodesMap){
					log(g_logNodeStream, `送 SNAPSHOT_NODE_START 給 [handler] batchId: ${batchId}`);
					g_gathHandler.triggerEvent(protocol.nodeStream_2_gatherHandler.SNAPSHOT_NODE_START, JSON.stringify({batchId}));


					let block = null;
					let node = {id:v[0].id, blocks:{}, peers:v[1].peers};

					// blocks
					try{
						let path = v[0].blockPath;
						let blockHash = v[1].latestHash;
						let blockDB = await LevelKV.initFromPath(path);
						blockDB.initPath = path;

						node.latest = blockHash;
						log(g_logNodeStream, `第一個 blockHash: `, blockHash);
						while( blockHash != ZERO_HASH.toString() ){
							block = (await (await blockDB.get( blockHash )).toArray())[0];
							log(g_logNodeStream, `block: `, util.inspect(block, {depth: null}));
							node.blocks[blockHash.toString()] = block;

							blockHash = block.parent;
							log(g_logNodeStream, `下一個 blockHash: `, blockHash);
						}
						await blockDB.close();
					}catch(err){
						console.log("nodeStream error1");
						console.log(err);
					}

					// 解鎖
					try{
						log(g_logNodeStream, `送 UNLOCK_NODE_REQ 給 [node_${v[0].id.substr(34, 61)}]`);
						v[0].triggerEvent(protocol.nodeStream_2_node.UNLOCK_NODE_REQ, JSON.stringify({echo:{batchId}}));
					}catch(err){
						console.log("nodeStream error2");
						console.log(err);
					}

					// 拆解
					try{
						log(g_logNodeStream, `送 SNAPSHOT_NODE 給 [handler] `, node);
							const sentData = {batchId, nodeInfo:node};
						g_gathHandler.triggerEvent(protocol.nodeStream_2_gatherHandler.SNAPSHOT_NODE, Serialize(sentData));
					}catch(err){
						console.log("nodeStream error3");
						console.log(err);
					}

					log(g_logNodeStream, `送 SNAPSHOT_NODE_END 給 [handler] batchId: ${batchId}`);
					g_gathHandler.triggerEvent(protocol.nodeStream_2_gatherHandler.SNAPSHOT_NODE_END, JSON.stringify({batchId}));
				}
				log(g_logNodeStream, `送 SNAPSHOT_END 給 [handler] batchId: ${batchId}`);
				g_gathHandler.triggerEvent(protocol.nodeStream_2_gatherHandler.SNAPSHOT_END, JSON.stringify({batchId}));
				nodesMap.clear();
				delete g_snapshot[batchId];
			}
		})
		.connect(...connection);


		const nodeServer = new NetEvtServer();

		nodeServer
		.on("connected", (e)=>{
			const {type, sender, rawData} = e;
			log(g_logNodeStream, `node connected!`);


			if( g_gathHandler ){
				sender.triggerEvent(protocol.nodeStream_2_node.SERVER_OK);
			}else{
				g_nodesTemp.push(sender);
			}
		})
		.on("disconnected", (e)=>{
			console.log(e.type);
			const {type, sender, rawData} = e;
			log(g_logNodeStream, `node disconnected! ${sender.id.substr(34, 61)}`);

			let index = g_nodes.indexOf(sender);
			if( index > -1){
				g_nodes.splice(index, 1);
				log(g_logNodeStream, `remove ${sender.id.substr(34, 61)} successful`);
			}else{
				log(g_logNodeStream, `!!! 不該發生... ${sender.id.substr(34, 61)}`);
			}
		})
		.on(protocol.node_2_nodeStream.ADD_NODE, (e)=>{
			const {type, sender, rawData} = e;
			const {nodeId, blockPath} = JSON.parse(rawData);
			g_nodes.push(sender);
			log(g_logNodeStream, `[node] ADD_NODE, nodeId:${nodeId}, blockPath: ${blockPath}`);
			// log(g_logNodeStream, `[node][${.id.substr(34, 61);}] SNAPSHOT_READY_ACK, batchId:${batchId}`);
			sender.id = nodeId;
			sender.blockPath = blockPath;
		})
		.on(protocol.node_2_nodeStream.LOCK_NODE_ACK, (e)=>{
			const {type, sender, rawData} = e;
			const {peers, latest:latestHash, echo} = JSON.parse(rawData);
			const {batchId} = echo;
			log(g_logNodeStream, `[node_${sender.id.substr(34, 61)}] LOCK_NODE_ACK, batchId:${batchId}`);
			log(g_logNodeStream, `[node_${sender.id.substr(34, 61)}] LOCK_NODE_ACK, g_snapshot[batchId]: `, g_snapshot[batchId]);
			log(g_logNodeStream, `[node_${sender.id.substr(34, 61)}] LOCK_NODE_ACK, g_snapshot[batchId].nodes: `, g_snapshot[batchId].nodes);
			log(g_logNodeStream, `[node_${sender.id.substr(34, 61)}] LOCK_NODE_ACK, g_snapshot[batchId].states: `, g_snapshot[batchId].statesMap);
			g_snapshot[batchId].nodes.delete(sender.id);
			g_snapshot[batchId].statesMap.set(sender, {peers, latestHash});
			if( g_snapshot[batchId].nodes.size === 0 ){
				log(g_logNodeStream, `送 SNAPSHOT_READY_REQ 給 [handler] batchId: ${batchId}`);
				g_gathHandler.triggerEvent(protocol.nodeStream_2_gatherHandler.SNAPSHOT_READY_REQ, JSON.stringify({batchId}));
			}
		})
		.on(protocol.node_2_nodeStream.UNLOCK_NODE_ACK, (e)=>{
			const {type, sender, rawData} = e;
			const {peers, latest, echo} = JSON.parse(rawData);
			const {batchId} = echo;
			log(g_logNodeStream, `[node_${sender.id.substr(34, 61)}] UNLOCK_NODE_ACK, batchId:${batchId}`);
		})
		.listen(...listen);
	}catch(err){
		console.log(`!!! nodeStream exception`);
		console.log(err);
	}

	function log(logObj, ...args){
		let t = new Date();
		let month = t.getMonth()+1;
		month = month.toString().padStart(2, 0);
		let tStr = `[${t.getFullYear()}-${month}-${t.getDate().toString().padStart(2, 0)} ${t.getHours().toString().padStart(2, 0)}:${t.getMinutes().toString().padStart(2, 0)}:${t.getSeconds().toString().padStart(2, 0)}.${t.getMilliseconds().toString().padStart(3, 0)}]`;

		for(let arg of args){
			tStr += arg;
		}
		// console.log(tStr);
		fs.appendFileSync(logObj, tStr+'\r\n', 'utf8');
	}
})();