(()=>{
	"use strict";

	module.exports = Object.freeze({
		gather_2_summary:{
			GATHER_REGISTER_REQ: "gather-register-req",
			TXN_SCORE_REPORT: "txn-score-report"
		},
		summary_2_gather:{
			GATHER_REGISTER_ACK: "gather-register-ack",
			SNAPSHOT_NOTIFY: "snapshot-notify"
		},
		summary_2_gatherHandler:{
			SNAPSHOT_NOTIFY: "snapshot-notify",
			WATCHER_REQ: "watcher-req",
			GATHER_OFF: "gather-off"
		},
		gatherHandler_2_summary:{
			WATCHER_ACK: "watcher-ack",
			SNAPSHOT_END: "snapshot-end",
		},
		nodeStream_2_gatherHandler:{
			SNAPSHOT_READY_REQ: "snapshot-ready-req",
			SNAPSHOT_START: "snapshot-start",
			SNAPSHOT_NODE_START: "snapshot-node-start",
			SNAPSHOT_NODE: "snapshot-node",
			SNAPSHOT_NODE_END: "snapshot-node-end",
			SNAPSHOT_END: "snapshot-end",
		},
		gatherHandler_2_nodeStream:{
			SNAPSHOT_READY_ACK: "snapshot-ready-ack",
		},
		gather_2_nodeStream:{
			SNAPSHOT_START: "snapshot-start"
		},
		node_2_nodeStream:{
			ADD_NODE: "ADD_NODE",
			LOCK_NODE_ACK: "LOCK_NODE_ACK",
			UNLOCK_NODE_ACK: "UNLOCK_NODE_ACK",
		},
		nodeStream_2_node:{
			LOCK_NODE_REQ: "LOCK_NODE_REQ",
			UNLOCK_NODE_REQ: "UNLOCK_NODE_REQ",
			SERVER_OK: "SERVER_OK",
		},
		gather_2_txnScore:{
			SNAPSHOT_START: "snapshot-start"
		},
		txnScore_2_gather:{
			TXN_SCORE_REPORT: "txn-score-report"
		},
		node_2_txnScore:{
			ADD_NODE: "ADD_NODE",
			ADD_TXN: "ADD_TXN"
		},
		summary_2_blockScore:{
			ASSIGN_JOB: "assign-job"
		},
		blockScore_2_summary:{
			BLOCK_SCORE_REPORT: "block-score-report"
		}
	});
})();