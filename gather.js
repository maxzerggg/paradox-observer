(()=>{
	"use strict";

	const {NetEvtClient} 			= require( 'netevt' );
	const worker  					= require( 'worker_threads' );
	const protocol					= require( './protocol');
	const fs						= require( 'fs' );
	const {args:{OBSERVER}, tag}	= require( './lib/pemu-tiny' );
	const moment					= require( 'moment' );
	const {summary, nodeStream, txnScore} = OBSERVER;

	// global variable
	let g_nodeStreamThread 	= null;
	let g_txnScoreThread 	= null;
	let g_logPath			= null;
	let g_logGather			= null;


	try{
		const summaryClient = new NetEvtClient();
		summaryClient
		.on("connected", async (e)=>{
			const {type, sender, rawData} = e;
			log(g_logGather, `summary connected! ${sender}`);

			g_logPath = `${tag}_${moment().format('YYYY-MM-DD hh-mm-ss')}`;
			log(g_logGather, `送 GATHER_REGISTER_REQ 給 [summary] gatherId: ${tag}`);
			sender.triggerEvent(protocol.gather_2_summary.GATHER_REGISTER_REQ, JSON.stringify({gatherId:tag, logPath: g_logPath}));
		})
		.on(protocol.summary_2_gather.GATHER_REGISTER_ACK, async(e)=>{
			const {type, sender, rawData} = e;
			const {handlerPort, logPath:g_logPath} = JSON.parse(rawData);

			await ___CREATE_DIR(g_logPath);
			g_logGather	= fs.openSync(`${g_logPath}/Log_${tag}.txt`, "a");
			log(g_logGather, `[summary] GATHER_REGISTER_ACK, handlerPort:${handlerPort}`);

			let th1Ready = false;
			let th2Ready = false;

			// create nodeStream
			g_nodeStreamThread = new worker.Worker("./nodeStream.js", {workerData: {gatherId:tag, connection:[handlerPort, summary[1]], listen:nodeStream, logPath:g_logPath}});
			g_nodeStreamThread.on("online", (e)=>{
				th1Ready = true;
			});

			// create txnScore
			g_txnScoreThread = new worker.Worker("./txnScore.js", {workerData: {gatherId:tag, listen:txnScore, logPath:g_logPath}});
			g_txnScoreThread
				.on("online", (e)=>{
					th2Ready = true;
				})
				.on("message", (jsonStr)=>{
					const {type, data} = JSON.parse(jsonStr);
					switch (type) {
						case protocol.txnScore_2_gather.TXN_SCORE_REPORT:
						{
							log(g_logGather, `[txnScore] TXN_SCORE_REPORT`);
							sender.triggerEvent(protocol.gather_2_summary.TXN_SCORE_REPORT, JSON.stringify({result:data.result}));
						}
							break;
					}
				})

			let threadReadyCheckTimer = setInterval(()=>{
				if( th1Ready && th2Ready ){
					log(g_logGather, `gather's nodeStream & txnScore both are ready`);
					clearInterval(threadReadyCheckTimer);
					return;
				}
			}, 0);
		})
		.on(protocol.summary_2_gather.SNAPSHOT_NOTIFY, (e)=>{
			const {type, sender:summary, rawData} = e;
			const {batchId} = JSON.parse(rawData);
			log(g_logGather, `[summary] SNAPSHOT_NOTIFY, batchId:${batchId}`);

			log(g_logGather, `送 SNAPSHOT_START 給 [nodeStream] batchId: ${batchId}`);
			g_nodeStreamThread.postMessage(JSON.stringify({type:protocol.gather_2_nodeStream.SNAPSHOT_START, data:{batchId}}));
			log(g_logGather, `送 SNAPSHOT_START 給 [txnScore] batchId: ${batchId}`);
			g_txnScoreThread.postMessage(JSON.stringify({type:protocol.gather_2_txnScore.SNAPSHOT_START, data:{batchId}}));
		})
		.connect(...summary);
	}catch(err){
		console.log(`!!! gather exception`);
		console.log(err);
	}
	function ___CREATE_DIR (path, level=0){
		return new Promise(async(resolve)=>{
			let temp = path.split('/');
			if( level >= temp.length ){
				resolve();
				return;
			}

			let dir = "";
			for(let i=0; i<=level; i++){
				let mark = i===0 ? "" : "/";
				dir = dir+mark+temp[i];
			}
			if( !fs.existsSync(dir) ){
				fs.mkdirSync(dir);
			}
			await ___CREATE_DIR(path, level+1);
			resolve();
		});
	}

	function log(logObj, ...args){
		if( !logObj )	return;

		let t = new Date();
		let month = t.getMonth()+1;
		month = month.toString().padStart(2, 0);
		let tStr = `[${t.getFullYear()}-${month}-${t.getDate().toString().padStart(2, 0)} ${t.getHours().toString().padStart(2, 0)}:${t.getMinutes().toString().padStart(2, 0)}:${t.getSeconds().toString().padStart(2, 0)}.${t.getMilliseconds().toString().padStart(3, 0)}]`;

		for(let arg of args){
			tStr += arg;
		}
		// console.log(tStr);
		fs.appendFileSync(logObj, tStr+'\r\n', 'utf8');
	}
})();