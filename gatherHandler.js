(()=>{
	"use strict";

	const {NetEvtServer} 	= require( 'netevt' );
	const worker  			= require( 'worker_threads' );
	const {LevelKV} 		= require( 'levelkv' );
	const fs				= require( 'fs' );
	const protocol			= require( './protocol');
	const moment			= require( 'moment' );
	const { UInt64, Binary, Serialize, Deserialize } = require('beson');


	// global variable
	const {gatherId, listenPort, savePath, logPath} = worker.workerData;
	let g_snapshot 		= new Map();
	let g_logHandler	= fs.openSync(`${logPath}/Log_${gatherId}_handler.txt`, "a");

	try{
		let nodeStreamServer = new NetEvtServer();

		nodeStreamServer
		.on(protocol.nodeStream_2_gatherHandler.SNAPSHOT_READY_REQ, async (e)=>{
			const {type, sender, rawData} = e;
			const {batchId} = JSON.parse(rawData);
			const buildPath = `${savePath}/snapshot_${___EPOCH_TO_LOCAL(batchId)}/${gatherId}`;
			log(g_logHandler, `[nodeStream] SNAPSHOT_READY_REQ, batchId: ${batchId}`);
			let result = true;
			if( !fs.existsSync(buildPath) ){
				try{
					await ___CREATE_DIR(buildPath);
					log(g_logHandler, `g_snapshot 新增: 建立資料夾 ${buildPath}`);
					let db = await LevelKV.initFromPath(buildPath);
					db.initPath = buildPath;
					log(g_logHandler, `g_snapshot 新增: levelKV ${db}`);
					g_snapshot.set(batchId, {db});
					log(g_logHandler, `g_snapshot 新增: Map(${batchId}, db)`);
				}catch(err){
					result = false;
					log(g_logHandler, `!!! gatherHandler(${gatherId}) SNAPSHOT_READY_REQ build path(${buildPath}) fail`);
					// throw new Error(err);
				}
			}
			log(g_logHandler, `送 SNAPSHOT_READY_ACK 給 [nodeStream] batchId: ${batchId}, result: ${result}`);
			sender.triggerEvent(protocol.gatherHandler_2_nodeStream.SNAPSHOT_READY_ACK, JSON.stringify({batchId, result}));
		})
		.on(protocol.nodeStream_2_gatherHandler.SNAPSHOT_START, async (e)=>{
			const {type, sender, rawData} = e;
			const {batchId, nodesNum} = JSON.parse(rawData);
			log(g_logHandler, `[nodeStream] SNAPSHOT_START, batchId: ${batchId}, nodesNum: ${nodesNum}`);
			let snapshot = g_snapshot.get(batchId);
			snapshot.nodesTotalNum = nodesNum;
			snapshot.nodesNum = 0;
			snapshot.timeStart = Date.now();
		})
		.on(protocol.nodeStream_2_gatherHandler.SNAPSHOT_NODE_START, async (e)=>{
			const {type, sender, rawData} = e;
			const {batchId} = JSON.parse(rawData);
			log(g_logHandler, `[nodeStream] SNAPSHOT_NODE_START, batchId: ${batchId}`);
			// let snapshot = g_snapshot.get(batchId);
			// snapshot.nodeStr = "";
		})
		.on(protocol.nodeStream_2_gatherHandler.SNAPSHOT_NODE, async (e)=>{
			const {type, sender, rawData} = e;
			const {batchId, nodeInfo} = Deserialize(rawData);

			try{
				log(g_logHandler, `[nodeStream] SNAPSHOT_NODE, batchId: ${batchId}, nodeInfo: `, nodeInfo);
				// log(g_logHandler, `[nodeStream] SNAPSHOT_NODE, typeof nodeStr: ${typeof nodeStr}`);
				let snapshot = g_snapshot.get(batchId);
				// log(g_logHandler, `[nodeStream] SNAPSHOT_NODE, typeof nodeStr: ${typeof (snapshot.nodeStr)}`);
				// log(g_logHandler, `[nodeStream] SNAPSHOT_NODE 收完資料前: `, snapshot.nodeStr);
				snapshot.nodeInfo = nodeInfo;
				// log(g_logHandler, `[nodeStream] SNAPSHOT_NODE 收完資料後: `, JSON.stringify( snapshot.nodeInfo, null, 4));
			}catch(err){
				console.log("handler error1");
				console.log(err);
			}
		})
		.on(protocol.nodeStream_2_gatherHandler.SNAPSHOT_NODE_END, async (e)=>{
			const {type, sender, rawData} = e;
			const {batchId} = JSON.parse(rawData);
			log(g_logHandler, `[nodeStream] SNAPSHOT_NODE_END, batchId: ${batchId}`);

			try{
				let snapshot = g_snapshot.get(batchId)
				// log(g_logHandler, `snapshot.nodeStr: `, snapshot.nodeStr);
				let nodeInfo = snapshot.nodeInfo;
				snapshot.nodesNum += 1;
				log(g_logHandler, `[nodeStream] SNAPSHOT_NODE_END, batch(${batchId}) 新增一個node: db(${snapshot.db.initPath}) 新增一個node, id: ${nodeInfo.id}`);
				await snapshot.db.put(nodeInfo.id, nodeInfo);
			}catch(err){
				console.log("handler error2");
				console.log(err);
				// log(g_logHandler, `!!! gatherHandler(${gatherId}) batchId(${batchId}) nodeStr(${snapshot.nodeStr}) JSON.parse fail`);
				// throw new Error(err);
			}
		})
		.on(protocol.nodeStream_2_gatherHandler.SNAPSHOT_END, async (e)=>{
			const {type, sender, rawData} = e;
			const {batchId} = JSON.parse(rawData);
			log(g_logHandler, `[nodeStream] SNAPSHOT_END, batchId: ${batchId}`);

			// NOTICE: 2018/11/30 這個idle是因為底層levelKV的operation彼此會互相卡到影響, 所以在應用層先自己idle慢點
			// 以後底層修正後要拿掉!!
			// 科怪跟amy說修正了  2018/12/12
			// await _idle(1);

			let snapshot = g_snapshot.get(batchId);
			if(snapshot.nodesNum !== snapshot.nodesTotalNum){
				log(g_logHandler, `!!! gatherHandler(${gatherId}) batchId(${batchId}) nodesNum(${snapshot.nodesNum}) !== nodesTotal(${snapshot.nodesTotalNum})`);
			}

			let before = await snapshot.db.get();
			log(g_logHandler, `[nodeStream] SNAPSHOT_END, before: ${before.length}  `, snapshot.db.initPath);
			await snapshot.db.close();

			g_snapshot.delete(batchId);
			log(g_logHandler, `[nodeStream] SNAPSHOT_END, batchId: ${batchId} delete`);

			// NOTICE: 2018/11/30 這個idle是因為上面levelKV才剛釋放資料夾底下檔案的point, 接著太快改資料夾檔名, OS會拒絕
			await _idle(1);

			const path = `${savePath}/snapshot_${___EPOCH_TO_LOCAL(batchId)}/${gatherId}`;
			fs.renameSync(path, `${path}_[done]`);

			log(g_logHandler, `送 SNAPSHOT_END 給 [summary] batchId: ${batchId}, gatherId: ${gatherId}`);
			worker.parentPort.postMessage(JSON.stringify({type:protocol.gatherHandler_2_summary.SNAPSHOT_END, data:{batchId, gatherId, nodesNum:snapshot.nodesTotalNum}}));

			snapshot = null;
		})
		.listen(listenPort, "localhost");


		worker.parentPort.on("message", async (jsonStr)=>{
		const {type, data} = JSON.parse(jsonStr);
		switch (type) {
			case protocol.summary_2_gatherHandler.SNAPSHOT_NOTIFY:
			{
				const {batchId} = data;
				const buildPath = `${savePath}/snapshot-${batchId}/${gatherId}`;
				log(g_logHandler, `[summary] SNAPSHOT_NOTIFY, batchId: ${batchId}`);
				// if( !fs.existsSync(buildPath) ){
				// 	try{
				// 		await ___CREATE_DIR(buildPath);
				// 		log(g_logHandler, `g_snapshot 新增: 建立資料夾 ${buildPath}`);
				// 		let db = await LevelKV.initFromPath(buildPath);
				// 		log(g_logHandler, `g_snapshot 新增: levelKV ${db}`);
				// 		g_snapshot.set(batchId, {db});
				// 		log(g_logHandler, `g_snapshot 新增: Map(${batchId}, db)`);
				// 	}catch(err){
				// 		log(g_logHandler, err);
				// 		log(g_logHandler, `!!! gatherHandler(${gatherId}) SNAPSHOT_NOTIFY build path(${buildPath}) fail`);
				// 		// throw new Error(err);
				// 	}
				// }
			}
			break;
			case protocol.summary_2_gatherHandler.WATCHER_REQ:
			{
				const {watchId} = data;
				log(g_logHandler, `[summary] WATCHER_REQ, watchId: ${watchId}`);
				let nowTime = Date.now();
				let watcher = {};
				log(g_logHandler, `[summary] WATCHER_REQ, 此時 g_snapshot(${g_snapshot.size})`);
				for(let v of g_snapshot){
					let batchId = v[0];
					let info = {
						nodesTotalNum: v[1].nodesTotalNum,
						nodesNum: v[1].nodesNum,
						passTime: Math.round((nowTime-v[1].timeStart)/1000)
					};
					watcher[batchId] = info;
				}
				log(g_logHandler, `送 WATCHER_ACK 給 [summary] watchId: ${watchId}, gatherId: ${gatherId}`);
				worker.parentPort.postMessage(JSON.stringify({type:protocol.gatherHandler_2_summary.WATCHER_ACK, data:{watchId, gatherId, watcher}}));
			}
			break;
			case protocol.summary_2_gatherHandler.GATHER_OFF:
			{
				// 自滅
				fs.closeSync( g_logHandler );
				process.exit(0);
			}
			break;
		}
	})
	}catch(err){
		console.log(`!!! handler exception`);
		console.log(err);
	}



	function ___CREATE_DIR (path, level=0){
		return new Promise(async(resolve)=>{
			let temp = path.split('/');
			if( level >= temp.length ){
				resolve();
				return;
			}

			let dir = "";
			for(let i=0; i<=level; i++){
				let mark = i===0 ? "" : "/";
				dir = dir+mark+temp[i];
			}
			if( !fs.existsSync(dir) ){
				fs.mkdirSync(dir);
			}
			await ___CREATE_DIR(path, level+1);
			resolve();
		});
	}
	function ___EPOCH_TO_LOCAL(epoch){
		return moment(epoch).format('YYYY-MM-DD hh-mm-ss');
	}
	function _idle(second){
		return new Promise((resolve)=>{
			setTimeout(()=>{
				resolve();
			}, Math.floor(second*1000));
		});
	}

	function log(logObj, ...args){
		let t = new Date();
		let month = t.getMonth()+1;
		month = month.toString().padStart(2, 0);
		let tStr = `[${t.getFullYear()}-${month}-${t.getDate().toString().padStart(2, 0)} ${t.getHours().toString().padStart(2, 0)}:${t.getMinutes().toString().padStart(2, 0)}:${t.getSeconds().toString().padStart(2, 0)}.${t.getMilliseconds().toString().padStart(3, 0)}]`;

		for(let arg of args){
			tStr += arg;
		}
		// console.log(tStr);
		fs.appendFileSync(logObj, tStr+'\r\n', 'utf8');
	}
})();